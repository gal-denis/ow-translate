<?php
namespace app\components;


class LangHelper
{
    public static function getLabelByCode($code, $langs)
    {
        foreach($langs as $key => $value){
            if($code == $key) {
                return $value;
            }
        }
        
        return null;
    }

    public static function isRtlLang($code, $rtlLangs)
    {
        return array_search($code, $rtlLangs) !== false;
    }

    public static function getIsoCode($code, $isoLangs)
    {
        if(isset($isoLangs[$code])) {
            return $isoLangs[$code];
        }
        
        return null;
    }
}