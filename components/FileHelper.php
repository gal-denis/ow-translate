<?php
/**
 * Created by PhpStorm.
 * User: PRODEN
 * Date: 11.12.2017
 * Time: 8:28
 */

namespace app\components;


class FileHelper
{
    public static function removeFilesInDirectory($dir)
    {
        $files = glob($dir.'/*');
        foreach($files as $file){
            if(is_file($file)) {
                unlink($file);
            }
        }
    }
}