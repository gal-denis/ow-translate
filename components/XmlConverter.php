<?php
namespace app\components;

use DOMDocument;

class XmlConverter
{
    public function getLabel($xml)
    {
        if(!empty($xml['label'])) {
            return (string) $xml['label'];
        }
        
        return null;
    }

    public function xmlToStrings($xml)
    {
        $doc = '';

        $label = $this->getLabel($xml);
        if(!empty($label)) {
            $label = $this->encodeStringsValue($label);
            $doc .= "\"private_app_document_label\" = \"{$label}\";\r";
        }

        foreach ($xml->key as $key) {
            $k = (string) $key['name'];
            $v = (string) $key->value;
            $v = $this->encodeStringsValue($v);

           $doc .= "\"{$k}\" = \"{$v}\";\r";
        }
        
        return $doc;
    }

    public function stringsToXml($data, $path, $meta)
    {
        $strings = explode ("\n", $data);

        $arr = [];
        foreach ($strings as $string) {
            if(empty($string)) {
                continue;
            }

            preg_match('/^"(.+)" = "(.+)";$/', trim($string), $matches);
            $arr[$matches[1]] = $matches[2];
        }

        $label = '';
        if(isset($arr['private_app_document_label'])) {
            $label = $arr['private_app_document_label'];
            unset($arr['private_app_document_label']);
        }

        $xml = new DOMDocument('1.0', 'utf-8');

        $prefix = $xml->createElement("prefix");
        $prefix->setAttribute('name', $meta['filename']);
        $prefix->setAttribute('label', $label);
        $prefix->setAttribute('language_tag',  $meta['lang-code']);
        $prefix->setAttribute('language_label',  $meta['lang-label']);

        foreach ($arr as $k => $i) {
            $key = $xml->createElement("key");
            $key->setAttribute('name', $k);
            $value = $xml->createElement("value", $i);
            $key->appendChild($value);
            $prefix->appendChild($key);
        }

        $xml->appendChild($prefix);
        $xml->save($path . DIRECTORY_SEPARATOR . $meta['filename'] . '.xml');



        $xml = new DOMDocument('1.0', 'utf-8');
        $language = $xml->createElement('language');
        $language->setAttribute('tag', $meta['lang-code']);
        $language->setAttribute('label', $meta['lang-label']);
        $language->setAttribute('rtl', $meta['lang-rtl'] ? 1 : 0);
        $xml->appendChild($language);
        $xml->save($path . DIRECTORY_SEPARATOR . 'language.xml');
        
        return true;
    }

    private function encodeStringsValue($v)
    {
        $v = trim($v);
        $v = str_replace('"', '\"', $v);
        $v = str_replace(["\r\n", "\r", "\n"], '\n', $v);

        return $v;
    }
}