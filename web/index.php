<?php
use app\components\FileHelper;
use app\components\LangHelper;
use app\components\XmlConverter;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App as SlimApp;
use Slim\Views\PhpRenderer;
use Stichoza\GoogleTranslate\TranslateClient;

require '../vendor/autoload.php';

require '../config/languages.php';
require '../config/iso-lang-code.php';
require '../config/rtl.php';

/**
 * @var array $lang
 * @var array $isoLang
 * @var array $rtlLang
 */

$app = new SlimApp([
    'settings' => [
        'displayErrorDetails' => true
    ]
]);

$container = $app->getContainer();

$container['view'] = new PhpRenderer("../views/");
$container['xmlConverter'] = new XmlConverter();
$container['translate'] = new TranslateClient();
$container['files_folder'] = __DIR__ . '/translate';

$container['languages'] = $lang;
$container['isoLang'] = $isoLang;
$container['rtlLang'] = $rtlLang;

$container['errorHandler'] = function ($container) {
    return function ($request, $response, $exception) use ($container) {
        /**
         * @var $request \Slim\Http\Request
         * @var $response \Slim\Http\Response
         * @var $exception Exception
         */

        // retrieve logger from $container here and log the error
        $response->getBody()->rewind();

        return $response->withStatus(500)
            ->withHeader('Content-Type', 'text/html')
            ->write("Fatal error! Message: " . $exception->getMessage());
    };
};


$app->get('/', function (Request $request, Response $response) {
    return $this->view->render($response, "/index.php", [
        'langs' => $this->languages
    ]);
});

$app->post('/xml-to-string', function (Request $request, Response $response) {
    /** @var $request \Slim\Http\Request
     * @var $response \Slim\Http\Response */

    $folder = $this->get('files_folder');
    FileHelper::removeFilesInDirectory($folder);

    /** @var \Slim\Http\UploadedFile[] $uploadedFiles */
    $uploadedFiles = $request->getUploadedFiles();
    $uploadedFile = $uploadedFiles['file'];
    if ($uploadedFile->getError() !== UPLOAD_ERR_OK) {
        throw new Exception('XML file is not uploaded on server');
    }
    $xml = simplexml_load_file($uploadedFile->file);
    $content = $this->xmlConverter->xmlToStrings($xml);

    $newFilename = str_replace('.xml', '', $uploadedFile->getClientFilename()) . '.strings';
    $fullPath = $folder . DIRECTORY_SEPARATOR . $newFilename;
    $fp = fopen($fullPath, "wb");
    fwrite($fp, $content);
    fclose($fp);

    $res = $response->withHeader('Content-Description', 'File Transfer')
        ->withHeader('Content-Type', 'application/octet-stream')
        ->withHeader('Content-Disposition', 'attachment;filename="'.basename($fullPath).'"')
        ->withHeader('Expires', '0')
        ->withHeader('Cache-Control', 'must-revalidate')
        ->withHeader('Pragma', 'public')
        ->withHeader('Content-Length', filesize($fullPath));

    readfile($fullPath);

    return $res;
});

$app->post('/string-to-xml', function (Request $request, Response $response) {
    /** @var $request \Slim\Http\Request
     * @var $response \Slim\Http\Response */

    $folder = $this->get('files_folder');
    FileHelper::removeFilesInDirectory($folder);
    
    $targetLang = $request->getParam('target-lang');
    if(empty($targetLang)) {
        throw new Exception('Target language can not be empty');
    }

    $targetLang = trim($targetLang);

    if(!isset($this->languages[$targetLang])) {
        throw new Exception('Invalid target language');
    }

    /** @var \Slim\Http\UploadedFile[] $uploadedFiles */
    $uploadedFiles = $request->getUploadedFiles();
    $uploadedFile = $uploadedFiles['file'];
    if ($uploadedFile->getError() !== UPLOAD_ERR_OK) {
        throw new Exception('STRINGS file is not uploaded on server');
    }

    $metaData = [
        'lang-code' => LangHelper::getIsoCode($targetLang, $this->isoLang),
        'lang-label' => LangHelper::getLabelByCode($targetLang, $this->languages),
        'lang-rtl' => LangHelper::isRtlLang($targetLang, $this->rtlLang),
        'filename' => str_replace('.strings', '', $uploadedFile->getClientFilename())
    ];

    $fileContent = file_get_contents($uploadedFile->file);
    $this->xmlConverter->stringsToXml($fileContent, $folder, $metaData);

    $zip = new ZipArchive;
    $zipFilename = $metaData['filename'] . "_" . $metaData['lang-code'] . '.zip';
    $zipPath = $folder . DIRECTORY_SEPARATOR . $zipFilename;
    if ($zip->open($zipPath, ZipArchive::CREATE) === TRUE) {
        $zip->addFile($folder . DIRECTORY_SEPARATOR . $metaData['filename'] . '.xml', 'language_' . $metaData['lang-code'] . DIRECTORY_SEPARATOR . $metaData['filename'] . '.xml');
        $zip->addFile($folder . DIRECTORY_SEPARATOR . 'language.xml', 'language_' . $metaData['lang-code'] . DIRECTORY_SEPARATOR . 'language.xml');
        $zip->close();
    } else {
        throw new Exception('Can not create ZIP archive. You still can get localized files directly in web/translate folder');
    }

    return $response->withRedirect('/translate/' . $zipFilename);
});

$app->run();