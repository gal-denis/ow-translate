<?php
/**
 * @var $langs array
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>OXWALL Plugin translate</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        .tab-pane {
            border: 1px solid #ddd;
            padding: 10px;
            border-top: none;
        }
    </style>
</head>
<body>
    <div class="container" style="margin-top: 80px;">
        <ul class="nav nav-tabs nav-justified">
            <li class="active"><a data-toggle="tab" href="#panel1">.XML to .STRINGS</a></li>
            <li><a data-toggle="tab" href="#panel2">.STRINGS to .XML</a></li>
        </ul>

        <div class="tab-content">
            <div id="panel1" class="tab-pane in active">
                <form action="/xml-to-string" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                        <label for="file-upload"></label>
                        <input type="file" id="file-upload" name="file" accept="text/xml">
                    </div>

                    <hr />

                    <button type="submit" class="btn btn-success">Convert to .strings</button>
                </form>
            </div>
            <div id="panel2" class="tab-pane">
                <form action="/string-to-xml" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                        <label for="target-lang">Target language</label>
                        <select class="form-control" id="target-lang" name="target-lang">
                            <?php
                            foreach ($langs as $key => $lang) {
                                $selected = $key == 'ru' ? " selected=\"1\"" : '';
                                echo " <option value=\"{$key}\"{$selected}>{$lang}</option>";
                            }
                            ?>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label for="file-upload"></label>
                        <input type="file" id="file-upload" name="file" accept=".strings">
                    </div>

                    <hr />

                    <button type="submit" class="btn btn-success">Convert to .xml</button>
                </form>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>